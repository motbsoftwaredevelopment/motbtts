package org.museumofthebible.motbtts;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    TextToSpeech mTtsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //request permissions
        String[]permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        int status = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (status== PackageManager.PERMISSION_DENIED) ActivityCompat.requestPermissions(this, permissions, 1);

        //set volume of device
        android.media.AudioManager am = (android.media.AudioManager) getSystemService(Context.AUDIO_SERVICE);
        am.setStreamVolume(android.media.AudioManager.STREAM_MUSIC, 5, 0);

        //setup text to speech object
        mTtsHelper=new TextToSpeech(this,null);
    }

    private void speak() {
        mTtsHelper.speak("Turn left in 30 feet", TextToSpeech.QUEUE_ADD, null, null);
        mTtsHelper.speak("This is a test string", TextToSpeech.QUEUE_ADD, null, null);
        mTtsHelper.speak("2nd time for a test", TextToSpeech.QUEUE_ADD, null, null);
    }

    public void onClick(View view) {
        speak();
    }
}
